package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.AccountRepository;
import fr.univ.stjerome.directorymanager.model.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    @Spy
    private AccountServiceImpl accountService;

    @Test
    public void saveAccountTest(){
        Account account = new Account();
        accountService.save(account);
        verify(accountRepository).save(account);
    }

    @Test
    public void findAccountByIdTest(){
        int accountId = 100;
        Optional<Account> account = Optional.of(new Account());
        account.get().setId(accountId);
        doReturn(account).when(accountRepository).findById(accountId);
        assertEquals(accountId,accountService.findById(accountId).getId(),"the expected value doesn't equal the actual value");
    }

}