package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.GroupRepository;
import fr.univ.stjerome.directorymanager.model.Group;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {

    @Mock
    private GroupRepository groupRepository;

    @InjectMocks
    @Spy
    private GroupServiceImpl groupService;

    @Test
    public void saveGroupTest(){
        Group group = new Group();
        groupService.save(group);
        verify(groupRepository).save(group);
    }

    @Test
    public void findAllGroupsTest(){
        List<Group> groups = Collections.singletonList(new Group());
        doReturn(groups).when(groupRepository).findAll();
        assertEquals(groups,groupService.findAll());
    }

    @Test
    public void findGroupByIdTest(){
        int groupId = 12;
        Optional<Group> group = Optional.of(new Group());
        group.get().setId(groupId);
        doReturn(group).when(groupRepository).findById(groupId);
        assertEquals(groupId,groupService.findGroupById(groupId).getId(),"the expected value doesn't equal the actual value");

    }

}