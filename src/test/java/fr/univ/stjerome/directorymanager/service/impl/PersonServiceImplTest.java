package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.PersonRepository;
import fr.univ.stjerome.directorymanager.model.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    @Spy
    private PersonServiceImpl personService;

    @Test
    public void savePersonTest(){
        Person person = new Person();
        personService.save(person);
        verify(personRepository).save(person);
    }

    @Test
    public void findPersonByIdTest(){
        int personId = 154;
        Optional<Person> person = Optional.of(new Person());
        person.get().setId(personId);
        doReturn(person).when(personRepository).findById(personId);
        assertEquals(personId,personService.findById(personId).getId(),"the expected value doesn't equal the actual value");
    }

    @Test
    public void findAllPersonsTest(){
        List<Person> persons = Collections.singletonList(new Person());
        doReturn(persons).when(personRepository).findAll();
        assertEquals(persons,personService.findAll());
    }

    @Test
    public void findPersonByFirstNameAndLastNameTest(){
        String personFirstName = "firstName";
        String personLastName = "lastName";
        personService.find(personFirstName,personLastName);
        verify(personRepository).findAllByFirstNameContainingAndLastNameContainingAllIgnoreCase(personFirstName,personLastName);
    }

    @Test
    public void findPersonByEmailTest(){
        String email = "person@email.fr";
        Person person = new Person();
        person.setEmail(email);
        doReturn(person).when(personRepository).findByEmail(email);
        assertEquals(email,personService.findByEmail(email).getEmail(),"the expected value doesn't equal the actual value");
    }

}