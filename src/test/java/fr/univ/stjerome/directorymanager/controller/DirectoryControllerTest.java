package fr.univ.stjerome.directorymanager.controller;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import fr.univ.stjerome.directorymanager.dto.FindPersonForm;
import fr.univ.stjerome.directorymanager.dto.LoginForm;

import fr.univ.stjerome.directorymanager.model.Account;
import fr.univ.stjerome.directorymanager.model.Group;
import fr.univ.stjerome.directorymanager.model.Person;
import fr.univ.stjerome.directorymanager.model.User;
import fr.univ.stjerome.directorymanager.service.impl.AccountServiceImpl;
import fr.univ.stjerome.directorymanager.service.impl.GroupServiceImpl;
import fr.univ.stjerome.directorymanager.service.impl.PersonServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = DirectoryController.class)
class DirectoryControllerTest {

    @MockBean
    private GroupServiceImpl groupService;

    @MockBean
    private PersonServiceImpl personService;

    @MockBean
    private AccountServiceImpl accountService;

    @MockBean
    private User user;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void groupListTest() throws Exception {
        List<Group> groups = Collections.singletonList(new Group());

        doReturn(groups).when(groupService).findAll();

        mockMvc.perform(
            get("/")
        ).andExpect(status().isOk())
        .andExpect(view().name("/WEB-INF/jsp/group_list.jsp"))
        .andExpect(model().attributeExists("groups"))
        .andExpect(forwardedUrl("/WEB-INF/jsp/group_list.jsp"));
    }

    @Test
    public void groupDetailsTest() throws Exception {
        int groupId = 20;
        Group group = new Group();
        group.setId(groupId);

        doReturn(group).when(groupService).findGroupById(groupId);

        mockMvc.perform(
                get("/group/{id}",groupId)
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/group_details.jsp"))
                .andExpect(model().attributeExists("group"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/group_details.jsp"));
    }

    @Test
    public void personDetailsWithIdInPathTest() throws Exception {
        int personId = 20;
        Person person = new Person();
        person.setId(personId);

        doReturn(person).when(personService).findById(personId);

        mockMvc.perform(
                get("/persons/{id}",personId)
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/person_details.jsp"))
                .andExpect(model().attributeExists("person"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/person_details.jsp"));
    }

    @Test
    public void personDetailsTestSuccess() throws Exception {
        int personId = 20;
        String personEmail = "user@email.com";
        Person person = new Person();

        doReturn(personEmail).when(user).getEmail();
        doReturn(personId).when(user).getId();
        doReturn(person).when(personService).findById(personId);

        mockMvc.perform(
                get("/person")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/user_details.jsp"))
                .andExpect(model().attributeExists("person"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/user_details.jsp"));
    }

    @Test
    public void personDetailsTestFail() throws Exception {
        int personId = 20;
        Person person = new Person();

        doReturn(null).when(user).getEmail();
        doReturn(personId).when(user).getId();
        doReturn(person).when(personService).findById(personId);

        mockMvc.perform(
                get("/person")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/login.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"));
    }

    @Test
    public void findPersonGetTest() throws Exception {
        mockMvc.perform(
                get("/find")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/find_person.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/find_person.jsp"));
    }

    @Test
    public void findPersonPostTest() throws Exception {
        List<Person> persons = Collections.singletonList(new Person());
        FindPersonForm findPersonForm = new FindPersonForm();
        findPersonForm.setFirstName("firstName");
        findPersonForm.setLastName("lastName");

        doReturn(persons).when(personService).find(findPersonForm.getFirstName(),findPersonForm.getLastName());

        mockMvc.perform(
                post("/find")
                .param("firstName",findPersonForm.getFirstName())
                .param("lastName",findPersonForm.getLastName())
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/find_results.jsp"))
                .andExpect(model().attributeExists("persons"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/find_results.jsp"));
    }



    @Test
    public void loginGetTest() throws Exception {
        mockMvc.perform(
                get("/login")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/login.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"));
    }

    @Test
    public void loginPostTestSuccess() throws Exception {
        String login = "login";
        String password = "password";
        int personId = 50;
        LoginForm loginForm = new LoginForm(login,password);
        Person person = new Person();
        person.setId(personId);
        person.setEmail(login);
        Account account = new Account();
        account.setId(personId);
        account.setPassword(password);

        doReturn(person).when(personService).findByEmail(eq(loginForm.getLogin()));
        doReturn(account).when(accountService).findById(personId);

        assertEquals(account.getPassword(),loginForm.getPassword());

        mockMvc.perform(
                post("/login")
                .param("login",login)
                .param("password",password)
        ).andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void loginPostFailTest() throws Exception {
        String login = "login";
        String password = "password";
        int personId = 50;
        LoginForm loginForm = new LoginForm(login,password);

        doReturn(null).when(personService).findByEmail(eq(loginForm.getLogin()));
        doReturn(null).when(accountService).findById(personId);

        mockMvc.perform(
                post("/login")
                        .param("login",login)
                        .param("password",password)
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/login.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"));

    }

    @Test
    public void updateGetTest() throws Exception {
        mockMvc.perform(
                get("/update")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/update_user.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/update_user.jsp"));
    }

    @Test
    public void updatePostTest() throws Exception {
        String email = "user@email.com";
        Person person = new Person();
        Person updatedPerson = new Person();

        doReturn(email).when(user).getEmail();
        doReturn(updatedPerson).when(personService).findByEmail(email);
        doReturn(person).when(personService).save(person);

        mockMvc.perform(
                post("/update")
                        .param("firstName","userFirstName")
                        .param("lastName","userLastName")
                        .param("website","userWebsite")
                        .param("email",email)
        ).andExpect(status().isFound())
                .andExpect(redirectedUrl("/person"));
    }

    @Test
    public void changePasswordGetTest() throws Exception {
        mockMvc.perform(
                get("/changePassword")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/change_password.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/change_password.jsp"));
    }

    @Test
    public void changePasswordPostSuccessTest() throws Exception {
        mockMvc.perform(
                get("/changePassword")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/change_password.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/change_password.jsp"));
    }

    @Test
    public void changePasswordPostFailTest() throws Exception {
        mockMvc.perform(
                get("/changePassword")
        ).andExpect(status().isOk())
                .andExpect(view().name("/WEB-INF/jsp/change_password.jsp"))
                .andExpect(forwardedUrl("/WEB-INF/jsp/change_password.jsp"));
    }



}

