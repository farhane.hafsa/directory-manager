package fr.univ.stjerome.directorymanager.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "person")
public class Person {

    @Id
    private int id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    private String email;

    private Date birthday;

    private String website;

    private int personsGroupId;

    @OneToOne
    private Account account;

    public Person() { }

    public Person(String firstName, String lastName, String email, String website) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.website = website;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getPersonsGroupId() {
        return personsGroupId;
    }

    public void setPersonsGroupId(int personsGroupId) {
        this.personsGroupId = personsGroupId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
