package fr.univ.stjerome.directorymanager.dao;

import fr.univ.stjerome.directorymanager.model.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<Group , Integer> {

}
