package fr.univ.stjerome.directorymanager.dao;

import fr.univ.stjerome.directorymanager.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    List<Person> findAllByFirstNameContainingAndLastNameContainingAllIgnoreCase(String firstName, String lastName);

    Person findByEmail(String email);
}
