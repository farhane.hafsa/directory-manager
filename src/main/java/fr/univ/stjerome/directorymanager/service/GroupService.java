package fr.univ.stjerome.directorymanager.service;

import fr.univ.stjerome.directorymanager.model.Group;

import java.util.List;

public interface GroupService {

    Group save(Group group);

    List<Group> findAll();

    Group findGroupById(int id);

}
