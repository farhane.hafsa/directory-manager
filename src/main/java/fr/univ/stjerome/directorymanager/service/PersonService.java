package fr.univ.stjerome.directorymanager.service;

import fr.univ.stjerome.directorymanager.model.Person;

import java.util.List;

public interface PersonService {

    Person save(Person person);

    Person findById(int id);

    List<Person> findAll();

    List<Person> find(String fistName, String lastName);

    Person findByEmail(String email);
}
