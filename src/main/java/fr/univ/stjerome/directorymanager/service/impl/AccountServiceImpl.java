package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.AccountRepository;
import fr.univ.stjerome.directorymanager.model.Account;
import fr.univ.stjerome.directorymanager.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account findById(int id) {
        return accountRepository.findById(id).orElse(null);
    }
}
