package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.GroupRepository;
import fr.univ.stjerome.directorymanager.model.Group;
import fr.univ.stjerome.directorymanager.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl implements  GroupService {


    @Autowired
    private GroupRepository groupRepository;

    @Override
    public Group save(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public List<Group> findAll() {
        List<Group> groups = (List<Group>) groupRepository.findAll();
        return groups;
    }

    @Override
    public Group findGroupById(int id) {
        Group group = groupRepository.findById(id).orElse(null);
        return group;
    }
}
