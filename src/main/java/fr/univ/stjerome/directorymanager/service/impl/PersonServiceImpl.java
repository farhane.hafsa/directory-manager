package fr.univ.stjerome.directorymanager.service.impl;

import fr.univ.stjerome.directorymanager.dao.PersonRepository;
import fr.univ.stjerome.directorymanager.model.Person;
import fr.univ.stjerome.directorymanager.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person save(Person person) {
        return personRepository.save(person);
    }

    @Override
    public Person findById(int Id) {
        Person person = personRepository.findById(Id).orElse(null);
        return person;
    }

    @Override
    public List<Person> findAll() {
        List<Person> persons = (List<Person>) personRepository.findAll();
        return persons;
    }

    @Override
    public List<Person> find(String firstName, String lastName) {
        return personRepository.findAllByFirstNameContainingAndLastNameContainingAllIgnoreCase(firstName, lastName);
    }

    @Override
    public Person findByEmail(String email) {
        return personRepository.findByEmail(email);
    }
}
