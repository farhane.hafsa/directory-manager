package fr.univ.stjerome.directorymanager.service;

import fr.univ.stjerome.directorymanager.model.Account;

public interface AccountService {

    Account save(Account account);

    Account findById(int id);

}
