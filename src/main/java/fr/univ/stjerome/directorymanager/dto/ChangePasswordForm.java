package fr.univ.stjerome.directorymanager.dto;

public class ChangePasswordForm {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
