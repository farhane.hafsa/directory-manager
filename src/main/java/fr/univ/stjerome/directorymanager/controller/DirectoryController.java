package fr.univ.stjerome.directorymanager.controller;

import fr.univ.stjerome.directorymanager.dto.ChangePasswordForm;
import fr.univ.stjerome.directorymanager.dto.FindPersonForm;
import fr.univ.stjerome.directorymanager.dto.LoginForm;
import fr.univ.stjerome.directorymanager.dto.UserUpdateForm;
import fr.univ.stjerome.directorymanager.model.Account;
import fr.univ.stjerome.directorymanager.model.Group;
import fr.univ.stjerome.directorymanager.model.Person;
import fr.univ.stjerome.directorymanager.model.User;
import fr.univ.stjerome.directorymanager.service.AccountService;
import fr.univ.stjerome.directorymanager.service.GroupService;
import fr.univ.stjerome.directorymanager.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Random;

@Controller
public class DirectoryController {

    @Autowired
    GroupService groupService;

    @Autowired
    PersonService personService;

    @Autowired
    AccountService accountService;

    @Autowired
    User user;

    @GetMapping("/")
    public ModelAndView groupList() {
        List<Group> groups = groupService.findAll();
        return new ModelAndView("/WEB-INF/jsp/group_list.jsp", "groups", groups);
    }

    @GetMapping("/group/{id}")
    public ModelAndView groupDetails(@PathVariable Integer id) {
        Group group = groupService.findGroupById(id);
        return new ModelAndView("/WEB-INF/jsp/group_details.jsp", "group", group);
    }

    @GetMapping("/persons/{id}")
    public ModelAndView personDetails(@PathVariable Integer id) {
        Person person = personService.findById(id);
        return new ModelAndView("/WEB-INF/jsp/person_details.jsp", "person", person);
    }

    @GetMapping("/person")
    public ModelAndView personDetails() {
        if (user.getEmail() != null) {
            Person person = personService.findById(user.getId());
            return new ModelAndView("/WEB-INF/jsp/user_details.jsp", "person", person);
        }
        return new ModelAndView("/WEB-INF/jsp/login.jsp");
    }

    @PostMapping("/find")
    public ModelAndView findPerson(@ModelAttribute FindPersonForm findPersonForm) {
        List<Person> persons = personService.find(findPersonForm.getFirstName(), findPersonForm.getLastName());
        return new ModelAndView("/WEB-INF/jsp/find_results.jsp", "persons", persons);
    }

    @GetMapping("/find")
    public ModelAndView findPerson() {
        return new ModelAndView("/WEB-INF/jsp/find_person.jsp");
    }

    @GetMapping(value = "/login")
    public ModelAndView login() {
        user.setEmail(null);
        user.setFirstName(null);
        user.setLastName(null);
        return new ModelAndView("/WEB-INF/jsp/login.jsp");
    }

    @PostMapping(value = "/login")
    public ModelAndView login(@ModelAttribute LoginForm loginForm) {
        Person person = personService.findByEmail(loginForm.getLogin());
        if (person != null) {
            Account account = accountService.findById(person.getId());
            if (account != null) {
                if (account.getPassword().equals(loginForm.getPassword())) {
                    user.setId(person.getId());
                    user.setFirstName(person.getFirstName());
                    user.setLastName(person.getLastName());
                    user.setEmail(person.getEmail());
                    List<Group> groups = groupService.findAll();
                    return new ModelAndView("redirect:/", "groups", groups).addObject(user);
                }
            }
        }
        return new ModelAndView("/WEB-INF/jsp/login.jsp");
    }

    @GetMapping(value = "/update")
    public ModelAndView update() {
        return new ModelAndView("/WEB-INF/jsp/update_user.jsp");
    }

    @PostMapping(value = "/update")
    public ModelAndView update(@ModelAttribute UserUpdateForm userUpdateForm) {
        Person updatedPerson = personService.findByEmail(user.getEmail());
        if (!userUpdateForm.getFirstName().equals("")) {
            updatedPerson.setFirstName(userUpdateForm.getFirstName());
            user.setFirstName(userUpdateForm.getFirstName());
        }
        if (!userUpdateForm.getLastName().equals("")) {
            updatedPerson.setLastName(userUpdateForm.getLastName());
            user.setLastName(updatedPerson.getLastName());
        }
        if (!userUpdateForm.getEmail().equals("")) {
            updatedPerson.setEmail(userUpdateForm.getEmail());
        }
        if (!userUpdateForm.getWebsite().equals("")) {
            updatedPerson.setWebsite(userUpdateForm.getWebsite());
        }
        Person person = personService.save(updatedPerson);
        return new ModelAndView("redirect:/person", "person", person);
    }

    @GetMapping(value = "/changePassword")
    public ModelAndView changePassword() {
        return new ModelAndView("/WEB-INF/jsp/change_password.jsp");
    }

    @PostMapping(value = "/changePassword")
    public ModelAndView changePassword(@ModelAttribute ChangePasswordForm changePasswordForm) {
        Person person = personService.findByEmail(changePasswordForm.getEmail());
        if (person != null) {
            Account updatedAccount = accountService.findById(person.getId());
            if (updatedAccount != null) {
                //generate a random password of 6 characters
                String alphanum = "0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                String newPassword = "";
                for (int i = 0; i < 6; i++) {
                    Random r = new Random();
                    int index = r.nextInt(62);
                    newPassword += alphanum.charAt(index);
                }
                updatedAccount.setPassword(newPassword);
            }
            Account account = accountService.save(updatedAccount);
            return new ModelAndView("/WEB-INF/jsp/password.jsp", "account", account);
        }
        return new ModelAndView("redirect:/login");
    }

}
