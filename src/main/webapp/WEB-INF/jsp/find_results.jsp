<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 23/04/2020
  Time: 15:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
<head>
    <%@ include file="/WEB-INF/jsp/boostrap/head-bootstrap.jsp" %>
    <meta charset="UTF-8">
    <title>Hafsa FARHANE LAACHIRI</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css" />" type="text/css"/>
    <link rel="stylesheet" href="<c:url value="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
<%@ include file="/WEB-INF/jsp/navbar.jsp" %>
<div class="container">
    <h3><b>Résultats de votre recherche :</h3>
    <c:if test="${persons.size() != 0}">
        <table class="table table-hover bg-white">
            <thead class="table-color">
            <tr>
                <th>Identifiant</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>+</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${persons}" var="person">
                <tr>
                    <td>${person.id}</td>
                    <td>${person.lastName}</td>
                    <td>${person.firstName}</td>
                    <td><a href="/annuaire/persons/${person.id}" style="color: #EE4439;"> Afficher les informations</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
    <c:if test="${persons.size() == 0}">
        <p>Ce personne n'existe pas dans l'anuuaire</p>
    </c:if>
</div>
</body>
</html>
