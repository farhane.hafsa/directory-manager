<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 20/04/2020
  Time: 18:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
<head>
    <title>Hafsa FARHANE LAACHIRI</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<c:url value="/css/style.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <%@ include file="/WEB-INF/jsp/boostrap/head-bootstrap.jsp" %>
</head>
<body>
<%@ include file="/WEB-INF/jsp/navbar.jsp"%>
<div class="container page">
    <div class="row row1">
        <div class="col-md-4 offset-md-4">
            <div class="col1">
                <h3 >Liste des informations :</h3>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><b>Identifiant :</b><span class="infos-color"> ${person.id}</span></li>
                    <li class="list-group-item"><b>Nom :</b> <span class="infos-color">${person.lastName}</span></li>
                    <li class="list-group-item"><b>Prénom :</b> <span class="infos-color">${person.firstName}</span></li>
                    <li class="list-group-item"><b>Site Web :</b> <span class="infos-color">${person.website}</span></li>
                    <li class="list-group-item"><b>Date de naissance :</b> <span class="infos-color"><fmt:formatDate value="${person.birthday}" pattern="yyyy-MM-dd" /></span></li>
                    <li class="list-group-item"><b>Email :</b> <span class="infos-color">${person.email}</span></li>
                    <li class="list-group-item" ><b ><a href="/annuaire/update" style="color: #EE4439;" > --> Modifier Mes informations <-- </a></b></li>
                </ul>

            </div>
        </div>
    </div>
</div>
</body>
</html>
