<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 22/04/2020
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
<head>
    <title>Hafsa FARHANE LAACHIRI</title>
    <meta charset="UTF-8">
    <%@ include file="/WEB-INF/jsp/boostrap/head-bootstrap.jsp"%>
    <link rel="stylesheet" href="<c:url value="/css/login.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
<%@ include file="/WEB-INF/jsp/navbar.jsp"%>
<div class="container page">
    <div class="row row1">
        <div class="col-md-4 offset-md-4">
            <div class="col1">
                <h2 class="text-center">Ci-dessous votre nouveau mot de passe</h2>
                <p>Normalement il doit être envoyé par mail mais y'a que cette solution vu que les mails sont pas des vrai.</p>
                <form >
                    <div class="form-group">
                        <label >Nouveau mot de passe : ${account.password}</label>
                    </div>
                    <a href="/annuaire/login" style="color: white; " >Retourner à la page du login</a>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
