<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 20/04/2020
  Time: 17:41
  To change this template use File | Settings | File Templates.
--%>
<jsp:useBean id="user" scope="session" class="fr.univ.stjerome.directorymanager.model.User"/>
<div class="bandeau-notification">
    <ul class="navbar " style="background-color: #EE4439;" >
        <div class="icon-bar">
            <a href="/annuaire/" data-toggle="tooltip" data-placement="bottom" title="Acceuil"><i class="fa fa-home "></i></a>
            <a href="/annuaire/person" data-toggle="tooltip" data-placement="bottom" title="Profil"><i class="fa fa-user"></i></a>
            <a href="/annuaire/find" data-toggle="tooltip" data-placement="bottom" title="chercher une personne"><i class="fa fa-search"></i></a>
            <a href="/annuaire/login" data-toggle="tooltip" data-placement="bottom" title="Déconnexion"><c:out value="${user.firstName}" default="Salut"/>  <c:out value="${user.lastName}" default="Visiteur"/> <i class="fa fa-power-off"></i> </a>
        </div>
    </ul>
</div>


