<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 17/04/2020
  Time: 12:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
<head>
    <meta charset="UTF-8">
    <%@ include file="/WEB-INF/jsp/boostrap/head-bootstrap.jsp"%>
    <title>Hafsa FARHANE LAACHIRI</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
<%@ include file="/WEB-INF/jsp/navbar.jsp"%>
<div class="container">
    <h1> Annuaire : Liste des groupes </h1>
    <table class="table table-hover bg-white">
        <thead class="table-color">
        <tr>
            <th>ID du groupe</th>
            <th>Nom du groupe</th>
            <th>Nombre des personnes</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${groups}" var="group">
            <tr>
                <td>${group.id}</td>
                <td>${group.name}</td>
                <td>${group.persons.size()}</td>
                <td><a href="/annuaire/group/${group.id}" style="color: #EE4439;"> Plus de détails...</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
