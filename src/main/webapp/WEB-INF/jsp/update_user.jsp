<%--
  Created by IntelliJ IDEA.
  User: Hafsa
  Date: 20/04/2020
  Time: 20:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<html>
<head>
    <title>Hafsa FARHANE LAACHIRI</title>
    <meta charset="UTF-8">
    <%@ include file="/WEB-INF/jsp/boostrap/head-bootstrap.jsp"%>
    <link rel="stylesheet" href="<c:url value="/css/login.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
</head>
<body>
<%@ include file="/WEB-INF/jsp/navbar.jsp"%>
<div class="container page">
    <div class="row row1">
        <div class="col-md-4 offset-md-4">
            <div class="col1">
                <h2 class="text-center">Saisir vos informations !</h2>
                <form action="/annuaire/update" method="POST">
                    <div class="form-group">
                        <label >Prénom</label>
                        <input type="text" placeholder="" name="firstName" >
                    </div>
                    <div class="form-group">
                        <label >Nom</label>
                        <input type="text" placeholder="" name="lastName">
                    </div>
                    <div class="form-group">
                        <label >Asdresse Web</label>
                        <input type="text" placeholder="" name="website" >
                    </div>
                    <div class="form-group">
                        <label >Adresse Mail</label>
                        <input type="email" placeholder="" name="email">
                    </div>
                    <button type="submit" class="btn btn-lg btn-info">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
